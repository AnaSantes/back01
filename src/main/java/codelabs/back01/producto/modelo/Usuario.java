package codelabs.back01.producto.modelo;

public class Usuario {

    private String id;

    public Usuario() {
    }

    public Usuario(String userId){
        this.id = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
