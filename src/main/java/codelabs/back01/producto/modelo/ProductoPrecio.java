package codelabs.back01.producto.modelo;

public class ProductoPrecio {
    private long id;
    private double precio;

    public ProductoPrecio() {
    }

    public ProductoPrecio(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
