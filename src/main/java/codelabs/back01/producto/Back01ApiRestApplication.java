package codelabs.back01.producto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back01ApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Back01ApiRestApplication.class, args);
	}

}
