package codelabs.back01.producto.controlador;

import codelabs.back01.producto.modelo.Producto;
import codelabs.back01.producto.modelo.ProductoPrecio;
import codelabs.back01.producto.servicio.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${url.base}")
public class ControladorProducto {

    @Autowired
    private ProductoService productoService;

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        Producto pr = productoService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto  Producto) {
        productoService.addProducto(Producto);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productoService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        Producto pr = productoService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecio productoPrecioOnly, @PathVariable int id){
        Producto pr = productoService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productoService.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto  pr = productoService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsuario()!=null)
            return ResponseEntity.ok(pr.getUsuario());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
